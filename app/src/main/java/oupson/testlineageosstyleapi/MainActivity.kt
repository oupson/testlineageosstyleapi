package oupson.testlineageosstyleapi

import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import lineageos.style.StyleInterface

class MainActivity : AppCompatActivity() {
    companion object {
        private const val LINEAGEOS_STYLE_REQUEST_CODE = 1
    }
    private val styleInterface : StyleInterface by lazy {
        StyleInterface.getInstance(this)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (checkSelfPermission(StyleInterface.CHANGE_STYLE_SETTINGS_PERMISSION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(StyleInterface.CHANGE_STYLE_SETTINGS_PERMISSION), LINEAGEOS_STYLE_REQUEST_CODE)
        } else {
            load()
        }
    }

    private fun load() {
        radioGroup_accent.removeAllViews()
        when(styleInterface.globalStyle) {
            StyleInterface.STYLE_GLOBAL_AUTO_WALLPAPER -> radioGroup_global_style.check(radioButton_wallpaper.id)
            StyleInterface.STYLE_GLOBAL_AUTO_DAYTIME -> radioGroup_global_style.check(radioButton_daytime.id)
            StyleInterface.STYLE_GLOBAL_LIGHT -> radioGroup_global_style.check(radioButton_light.id)
            StyleInterface.STYLE_GLOBAL_DARK -> radioGroup_global_style.check(radioButton_dark.id)
        }

        val accents = hashMapOf<Int, String>()
        println("accent: ${styleInterface.accent}")
        styleInterface.trustedAccents.forEachIndexed { index, accent ->
            println("[$index] => $accent")
            try {
                val appName = if (accent == "lineageos") {
                    "Default"
                } else {
                    packageManager.getApplicationLabel(packageManager?.getApplicationInfo(accent, PackageManager.GET_META_DATA))
                }
                val id = View.generateViewId()
                radioGroup_accent.addView(
                    RadioButton(this).apply {
                        text = appName
                        setId(id)
                    }
                )
                accents[id] = accent
            } catch (e : Exception) {
                e.printStackTrace()
            }
        }

        if (styleInterface.accent == "")
            radioGroup_accent.check(accents.keys.toList()[0])
        else
            radioGroup_accent.check(accents.keys.toList()[accents.values.indexOf(styleInterface.accent)])

        radioGroup_global_style.setOnCheckedChangeListener { _, i ->
            when(i) {
                radioButton_wallpaper.id -> styleInterface.setGlobalStyle(StyleInterface.STYLE_GLOBAL_AUTO_WALLPAPER, packageName)
                radioButton_daytime.id -> styleInterface.setGlobalStyle(StyleInterface.STYLE_GLOBAL_AUTO_DAYTIME, packageName)
                radioButton_light.id -> styleInterface.setGlobalStyle(StyleInterface.STYLE_GLOBAL_LIGHT, packageName)
                radioButton_dark.id -> styleInterface.setGlobalStyle(StyleInterface.STYLE_GLOBAL_DARK, packageName)
            }
        }

        radioGroup_accent.setOnCheckedChangeListener { _, i ->
            if (accents[i] != styleInterface.accent)
                styleInterface.accent = accents[i]
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            LINEAGEOS_STYLE_REQUEST_CODE -> load()
        }
    }
}
